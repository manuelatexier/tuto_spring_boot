package com.ecommerce.microcommerce.model;

/**
 * Created by clari on 28/08/2018.
 */
public class MargeProduct {

    //Propriétés

    private String produit;
    private int valeurMarge;

    //Constructor

    public MargeProduct() {
    }

    public MargeProduct(String produit, int valeurMarge) {
        this.produit = produit;
        this.valeurMarge = valeurMarge;
    }


    //GET SET
    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public int getValeurMarge() {
        return valeurMarge;
    }

    public void setValeurMarge(int valeurMarge) {
        this.valeurMarge = valeurMarge;
    }
}
